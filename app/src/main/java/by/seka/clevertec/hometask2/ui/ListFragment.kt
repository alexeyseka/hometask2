package by.seka.clevertec.hometask2.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import by.seka.clevertec.hometask2.R
import by.seka.clevertec.hometask2.databinding.ListFragmentBinding
import by.seka.clevertec.hometask2.ui.adapter.ItemsAdapter

class ListFragment : Fragment() {

    private var _binding: ListFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ListFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter =
            ItemsAdapter { item, title, description -> onItemClick(item, title, description) }
        binding.recyclerView.adapter = adapter

    }

    private fun onItemClick(item: View, title: String, description: String) {

        parentFragmentManager
            .beginTransaction()
            .addSharedElement(item, ViewCompat.getTransitionName(item).toString())
            .addToBackStack(null)
            .replace(R.id.fragment_container_view, DetailsFragment.newInstance(title, description))
            .commit()
    }
}