package by.seka.clevertec.hometask2.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import by.seka.clevertec.hometask2.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}