package by.seka.clevertec.hometask2.ui.adapter

import android.view.View
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import by.seka.clevertec.hometask2.R
import by.seka.clevertec.hometask2.databinding.ItemCardBinding

class ItemsViewHolder(
    private val adapterOnClick: (View, String, String) -> Unit,
    private val binding: ItemCardBinding
) : RecyclerView.ViewHolder(binding.root) {


    fun bind() {
        val itemNumber = (this.bindingAdapterPosition + 1).toString()

        val titleText = "${itemView.context.getString(R.string.title)} $itemNumber"
        val descriptionText = "${itemView.context.getString(R.string.description)} $itemNumber"

        binding.apply {

            title.text = titleText
            description.text = descriptionText
        }

        ViewCompat.setTransitionName(binding.picture, titleText)

        itemView.setOnClickListener {
            adapterOnClick(binding.picture, titleText, descriptionText)
        }
    }
}