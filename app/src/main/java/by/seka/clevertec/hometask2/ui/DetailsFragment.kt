package by.seka.clevertec.hometask2.ui

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.transition.TransitionInflater
import by.seka.clevertec.hometask2.R
import by.seka.clevertec.hometask2.databinding.DetailsFragmentBinding
import kotlin.system.exitProcess

private const val TITLE_ARG = "title"
private const val DESCRIPTION_ARG = "description"

class DetailsFragment : Fragment() {

    private var titleString: String? = null
    private var descriptionString: String? = null

    private var _binding: DetailsFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            titleString = it.getString(TITLE_ARG)
            descriptionString = it.getString(DESCRIPTION_ARG)
        }
        sharedElementEnterTransition = TransitionInflater.from(requireContext())
            .inflateTransition(R.transition.fragment_transition)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DetailsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewCompat.setTransitionName(binding.fullPicture, titleString)

        with(binding) {

            detailsTitle.text = titleString
            detailsDescription.text = descriptionString

            toolbarBackButton.setOnClickListener {
                activity?.onBackPressed()
            }

            toolbarCloseButton.setOnClickListener {
                showAlertDialog()
            }
        }
    }

    private fun showAlertDialog() {
        AlertDialog.Builder(context)
            .setTitle("Exit")
            .setMessage("Are you sure?")
            .setPositiveButton("OK") { _, _ ->
                exitProcess(0)
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
            .show()
    }

    companion object {

        @JvmStatic
        fun newInstance(titleString: String, descriptionString: String) =
            DetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(TITLE_ARG, titleString)
                    putString(DESCRIPTION_ARG, descriptionString)

                }
            }
    }
}