package by.seka.clevertec.hometask2.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import by.seka.clevertec.hometask2.R
import by.seka.clevertec.hometask2.databinding.ItemCardBinding

class ItemsAdapter(private val adapterOnClick: (View, String, String) -> Unit) :
    RecyclerView.Adapter<ItemsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemCardBinding.inflate(layoutInflater, parent, false)
        binding.cardView.setBackgroundResource(R.drawable.card_background_stroke)
        return ItemsViewHolder(adapterOnClick, binding)
    }

    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {

        holder.bind()
    }

    override fun getItemCount(): Int {
        return 1000
    }
}